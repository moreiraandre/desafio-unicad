<?php

Route::get('/', function () {
    return redirect()->route('entrega.index');
});

Route::resource('entrega', 'EntregaController')->middleware(['web', 'auth']);

Route::auth();

Auth::routes();
