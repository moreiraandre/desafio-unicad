const mix = require('laravel-mix');

mix
    .scripts([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/popper.js/dist/umd/popper.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'resources/js/google-maps.js'
    ], 'public/js/app.js')
    .styles([
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'resources/css/google-maps.css'
    ], 'public/css/app.css')
    .browserSync('unicad.test');

if (mix.inProduction()) {
    mix.version();
}
