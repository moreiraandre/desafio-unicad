@extends('layouts.template')

@section('content')
    <div id="floating-panel">
        <a href="{{route('entrega.index')}}" class="btn btn-primary">Voltar</a>
        <br>
        <br>
        <p><strong>#</strong>{{$entrega->getKey()}} - <strong>Cliente: </strong>{{$entrega->cliente_nome}} - <strong>Data: </strong>{{$entrega->data_entrega->format('d/m/Y')}}</p>
        <p>"{{$entrega->partida}}" <strong>-></strong> "{{$entrega->destino}}"</p>

        <input type="hidden" id="start" value="{{$entrega->partida}}">
        <input type="hidden" id="end" value="{{$entrega->destino}}">

        <div id="map"></div>
    </div>
@endsection
