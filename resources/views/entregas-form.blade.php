@extends('layouts.template')

@section('content')
    {!! Form::open(['route'=>'entrega.store']) !!}
    <div class="form-group">
        <label>Nome</label>
        {!! Form::text('cliente_nome', null, ['class'=>'form-control form-control-sm'.($errors->has('cliente_nome') ?
        ' is-invalid' : ''), 'placeholder'=>'Informe seu nome completo']) !!}

        @if ($errors->has('cliente_nome'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('cliente_nome') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <label>Data entrega</label>
        {!! Form::date('data_entrega', null, ['class'=>'form-control form-control-sm'.($errors->has('data_entrega') ?
        ' is-invalid' : '')]) !!}

        @if ($errors->has('data_entrega'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('data_entrega') }}</strong>
            </span>
        @endif
    </div>
    <div class="row">
        <div class="col-sm">
            <div class="form-group">
                <label>Partida</label>
                {!! Form::text('partida', null, ['class'=>'form-control form-control-sm'.($errors->has('partida') ?
                ' is-invalid' : ''), 'id'=>'autocomplete']) !!}

                @if ($errors->has('partida'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('partida') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-sm">
            <div class="form-group">
                <label>Chegada</label>
                {!! Form::text('destino', null, ['class'=>'form-control form-control-sm'.($errors->has('partida') ?
                ' is-invalid' : ''), 'id'=>'autocomplete2']) !!}

                @if ($errors->has('destino'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('destino') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-sm btn-primary">Salvar</button>
    <a class="btn btn-sm btn-danger" href="{{route('entrega.index')}}">Cancelar</a>
    {!! Form::close() !!}
@endsection
