@extends('layouts.template')

@section('content')
    <a class="btn btn-sm btn-primary" href="{{route('entrega.create')}}">Nova entrega</a>
    <br>
    <br>

    <table class="table table-bordered table-striped table-hover table-sm">
        <thead>
        <tr>
            <th>#</th>
            <th>Cliente</th>
            <th>Data entrega</th>
            <th>Partida</th>
            <th>Destino</th>
            <th>Mapa</th>
        </tr>
        </thead>

        <tbody>
        @forelse($dados as $d)
            <tr>
                <td>{{$d->getKey()}}</td>
                <td>{{$d->cliente_nome}}</td>
                <td>{{$d->data_entrega->format('d/m/Y')}}</td>
                <td>{{$d->partida}}</td>
                <td>{{$d->destino}}</td>
                <td>
                    <a href="{{{route('entrega.show', $d->getKey())}}}">Mapa</a>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6">Não há dados!</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
