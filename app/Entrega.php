<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrega extends Model
{
    protected $fillable = [
        'cliente_nome',
        'data_entrega',
        'partida',
        'destino',
        'user_id',
    ];

    protected $dates = ['data_entrega'];
}
