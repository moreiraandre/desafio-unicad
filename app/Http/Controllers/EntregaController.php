<?php

namespace App\Http\Controllers;

use App\Entrega;
use App\Http\Requests\EntregaRequest;
use Illuminate\Http\Request;

class EntregaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dados = Entrega::all();
        return view('entregas-index', compact('dados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('entregas-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EntregaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntregaRequest $request)
    {
        $request->merge(['user_id'=>auth()->user()->getKey()]); // ADICIONANDO ID DO USUÁRIO LOGADO
        Entrega::create($request->all());
        return redirect()->route('entrega.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entrega  $entrega
     * @return \Illuminate\Http\Response
     */
    public function show(Entrega $entrega)
    {
        return view('mapa', compact('entrega'));
    }
}
